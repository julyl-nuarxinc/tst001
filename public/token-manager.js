/**
 * Created by julyl on 6/28/2016.
 */
// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

///////////////////////////////
// UI event handlers
///////////////////////////////
document.getElementById('startSigninMainWindow').addEventListener("click", startSigninMainWindow, false);
document.getElementById('endSigninMainWindow').addEventListener("click", endSigninMainWindow, false);
document.getElementById('checkIdToken').addEventListener("click", checkIdToken, false);

document.getElementById('startSignoutMainWindow').addEventListener("click", startSignoutMainWindow, false);

///////////////////////////////
// config
///////////////////////////////

var settings = {
    authority: 'http://qa-signin.anx.com/identity',
    client_id: '58967A76BD4F4A2A85A03A2F12BED859',
    redirect_uri: 'http://127.0.0.1:8080/user-manager-sample.html',
    post_logout_redirect_uri: 'http://127.0.0.1:8080/user-manager-sample.html',
    response_type: 'id_token token',
    scope: 'openid email roles api',
    request_state_store: sessionStorage,
    store: sessionStorage,
    popup_redirect_uri:'http://127.0.0.1:8080/user-manager-sample-popup.html',

    silent_redirect_uri:'http://127.0.0.1:8080/user-manager-sample-silent.html',
    automaticSilentRenew:true,

    filterProtocolClaims: true,
    loadUserInfo: true
};
var mgr = new OidcTokenManager(settings);

function startSigninMainWindow() {
    mgr.redirectForToken().then(function() {
        console.log("signinRedirect done");
    }).catch(function(err) {
        console.log(err);
    });
}

function endSigninMainWindow() {
    mgr.processTokenCallbackAsync().then(function () {
        console.log("signed in " + mgr.session_state);
        checkSessionState();
    }).catch(function (err) {
        console.log(err);
    });
}

function checkSessionState() {
    mgr.oidcClient.loadMetadataAsync().then(function (meta) {
        if (meta.check_session_iframe && mgr.session_state) {
            document.getElementById('rp').src = 'check_session.html#' +
                'session_state=' + mgr.session_state +
                '&check_session_iframe=' + meta.check_session_iframe +
                '&client_id=' + settings.client_id;
        }
        else {
            document.getElementById('rp').src = 'about:blank';
        }
    });

    window.onmessage = function (e) {
        if (e.origin === 'http://127.0.0.1:8080' && e.data === 'changed') {
            mgr.removeToken();
            mgr.renewTokenSilentAsync().then(function () {
                // Session state changed but we managed to silently get a new identity token, everything's fine
                console.log('renewTokenSilentAsync success');
            }, function () {
                // Here we couldn't get a new identity token, we have to ask the user to log in again
                console.log('renewTokenSilentAsync failed');
            });
        }
    }
}

function startSignoutMainWindow(){
    mgr.redirectForLogout().then(function(resp) {
        log("signed out", resp);
    }).catch(function(err) {
        log(err);
    });
};